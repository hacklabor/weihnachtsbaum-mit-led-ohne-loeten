# Weihnachtsbaum mit LED ohne Löten

Ein Weihnachtsbaum zum an malen aus dem Lasercutter mit blinkenden LEDs und Batterie, die über ein klebendes Kupferband verbunden werden.
Damit könnne Kleinkinder auch ein LED Projekt machen.
Der Weihnachtsbaum kann mit Tusche/Fingermalfarben/Farbstiften bemalt werden.

Auf der Rückseite ist die Markierung für die Beinchen der LEDs (kurz und lang), sowie Plus und Negative Seite für die Batterie.
